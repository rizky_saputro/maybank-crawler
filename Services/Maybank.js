require('dotenv').config();
const puppeteer = require('puppeteer');
const Axios = require('axios');
const moment = require('moment');
const promise = require('bluebird');
const fs = require('fs');
const MAYBANK_URL = 'https://www.maybank2u.com.my/home/m2u/common/login.do';
const SESSION_URL = 'https://www.maybank2u.com.my/home/m2u/common/mbbPortalAccess.do';
const SPENDING_PAGE = 'https://www.maybank2u.com.my/home/m2u/m1000/getCardsSpending.do';
const STATEMENTS_PDF = 'https://www.maybank2u.com.my/home/m2u/m1201/M2UEbppViewStatements.do';
const PROFILE_URL = 'https://www.maybank2u.com.my/home/m2u/m2003/m2uPersonalSettingsDetails.do';

const axios = Axios.create({
  headers: {
    'Host': 'www.maybank2u.com.my',
    'Connection': 'keep-alive',
    'Origin': 'https://www.maybank2u.com.my',
    'User-Agent': 'Mozilla/5.0 (Macintosh;Intel Mac OS X 10 _14_0) AppleWebKit/537.36(KHTML, like Gecko) Chrome/71.0.3563.0 Safari/537.36',
    'Content-Type': 'application/x-www-form-urlencoded',
    'Accept': "*/*",
    'Referer': 'https://www.maybank2u.com.my/home/m2u/common/dashboard/casa',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'en-US,en;q=0.9'
  }
})

class Maybank {
  constructor() {
    this.browser = null;
    this.page = null;
    this.session = null;
    this.cookies = null;
    this.Promise = promise;
    this.cache = {};
  }

  isScraping() {
    for (let key in this.cache) {
      let status = this.cache[key].status.toUpperCase();
      if (status === 'PENDING' || status === 'SCRAPING') {
        return {
          status: true,
          username: this.cache[key].username
        }
      }
    }
    return false;
  }

  async openPage() {
    this.browser = await puppeteer.launch({
      headless: true,
      slowMo: 150,
    });
    const context = await this.browser.createIncognitoBrowserContext();
    this.page = await context.newPage();
    await this.page.setViewport({
      width: 1366,
      height: 768
    })
    await this.page.goto(MAYBANK_URL, {
      timeout: 60000
    });
  }

  async setCookieString() {
    const cookies = await this.page.cookies();
    const cookieString = cookies
      .map(cookie => `${cookie.name}=${cookie.value}`)
      .join('; ');
    this.cookies = cookieString;
  }

  getStatus(key) {
    const result = { ...this.cache[key]
    }
    return result
  }

  async login(key, username, password) {
    try {
      this.cache[key] = {
        status: 'SCRAPING',
        username
      }
      await this.openPage()
      const session = this.page.waitForResponse(resp => {
        if (resp.url() === SESSION_URL) {
          return resp
        }
      }, {
        timeout: 20000
      });
      await this.page.type('.LoginUsername---login-input---1f7gK', username);
      await this.page.click('.LoginUsername---login-button---3Fl8E.LoginUsername---login-input-button---3yVaU', {
        delay: 25
      });
      await this.page.waitForSelector('.btn.btn-success.btn.btn-default', {
        visible: true
      });
      await this.page.click('.btn.btn-success.btn.btn-default', {
        delay: 300
      });
      await this.page.waitForSelector('.SecurityPhrase---login-input---AwPq0');
      await this.page.type('.SecurityPhrase---login-input---AwPq0', password);
      await this.page.click('.btn.btn-success.btn.btn-default', {
        delay: 300
      });
      const sessionData = await session;
      this.session = await sessionData.json();
      await this.setCookieString();

      await this.page.waitForNavigation({
        waitUntil: 'domcontentloaded'
      })

      await this.getCardsSpending(key);
      await this.getProfile(key)
      // await this.logout()
    } catch (e) {
      console.error(e);
    }
  }

  async getProfile(key){
    this.cache[key].status = 'SCRAPING';

    const BV_EngineID = this.session.BV_EngineID;
    const token = this.session.token;
    const BV_SessionID = this.session.BV_SessionID;
    const Cookie = this.cookies;
    const query = `BV_SessionID=${BV_SessionID}&BV_EngineID=${BV_EngineID}&SECONDARY_TOKEN=${token}`

    let body = await this.page.$('body');
    const name = await this.page.evaluate(document => {
      const fullname = document.querySelectorAll('.col-md-12')[5].innerText.split('\n')[1]
      return fullname
    }, body)

    const DOB = await axios.post(PROFILE_URL, query, {
      headers: {
        Cookie
      }
    })

    this.cache[key].profile = {
      name,
      DOB : DOB.data.personalInfo.dateOfBirth
    }

    this.cache[key].status = 'DONE';

  }

  async getCardsSpending(key) {
    const BV_EngineID = this.session.BV_EngineID;
    const token = this.session.token;
    const BV_SessionID = this.session.BV_SessionID;
    const Cookie = this.cookies;
    const promises = [];
    for (let i = 0; i < 12; i++) {
      let stringDate = moment(moment().subtract(i, 'month')).format('MMM%20YYYY');
      let date = moment(moment().subtract(i, 'month')).format('DD-MM-YYYY');
      let query = `range%5Bweek%5D=false&range%5Bmonth%5D=true&range%5Blabel%5D=${stringDate}&range%5Bvalue%5D=${date}&BV_EngineID=${BV_EngineID}&BV_SessionID=${BV_SessionID}`
      promises.push(query)
    }

    this.cache[key].cardSpending = []
    await this.Promise.mapSeries(promises, query => {
      return axios.post(SPENDING_PAGE, query, {
          headers: {
            BV_EngineID,
            token,
            BV_SessionID,
            Cookie
          }
        })
        .then(resp => {
          this.cache[key].cardSpending.push(resp.data)
        })
    })

    this.cache[key].status = 'DONE';
    return
    // return this.Promise.resolve()
  }

  async getSavingAccountPage() {
    const BV_EngineID = this.session.BV_EngineID;
    const BV_SessionID = this.session.BV_SessionID;
    const token = this.session.token;
    const Cookie = this.cookies;

    const query = `accountIdx=0&accountCode=11&accountType=S&stmtDate=&BV_SessionID=${BV_SessionID}&BV_EngineID=${BV_EngineID}`
    const response = await axios.post(STATEMENTS_PDF, query, {
      headers: {
        BV_EngineID,
        token,
        BV_SessionID,
        Cookie
      }
    });

    const availableList = response.data.statementList;

    this.Promise.mapSeries(availableList, item => {
      return Axios({
          method: 'GET',
          url: item.statementURL,
          responseType: 'stream',
          headers: {
            'Host': 'ebpp.maybank2u.com.my',
            'Connection': 'keep-alive',
            'Origin': 'https://www.maybank2u.com.my',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36(KHTML, like Gecko) Chrome/71.0.3563.0 Safari/537.36',
            'Accept': '*/*',
            'Referer': 'https://www.maybank2u.com.my/home/m2u/common/accountDetails/casa',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'en-US,en;q=0.9'
          }
        })
        .then(response => {
          const regex = /\//gm;
          const filename = item.label.replace(regex, '-');
          response.data.pipe(fs.createWriteStream(`./pdfs/${filename}.pdf`));
          response.data.on('end', () => {
            return this.Promise.resolve();
          })
        })
    });
  }

  async logout() {
    await this.page.click('.Header---profile---20pic', {
      delay: 500
    })

    await this.page.waitForSelector('div.col-xs-4.SideBar---logout_icon_wrapper---1GxNQ', {
      visible: true
    })

    await this.page.click('div.col-xs-4.SideBar---logout_icon_wrapper---1GxNQ', {
      delay: 500
    })
  }
}

module.exports = Maybank