const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const Maybank = require('./services/Maybank');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

let maybank = new Maybank();

const keyGen = (source, data) => {
  return Buffer.from(JSON.stringify({
    source,
    data
  })).toString('base64')
}

app.post('/', async (req, res) => {
  try {
    const {
      username,
      password,
    } = req.body;

    if (username && password) {
      let key = keyGen('MAYBANK', username);

      const isScraping = maybank.isScraping()
      if (isScraping && 
        isScraping.status === true && 
        isScraping.username !== username) {
          res.json({
            username: isScraping.username,
            err: 'currently scraping another username, retry later',
            statusCode: 422
          })
    } else if (maybank.cache[key]) {
      res.json({
        username,
        ...maybank.getStatus(key)
      })
    } else {
      maybank.login(key, username, password)
      res.json({
        username,
        ...maybank.getStatus(key)
      })
    }
  } else {
    res.json({
      status: 'FAILED',
      error: 'must provide username & password'
    })
  }


} catch (err) {
  console.log(err);
  res.status(400).send({
    error: err.message
  })
}
})

app.listen(3000, () => {
  console.log("scrape server running and accepting connections");
})